package com.saiba.app.review.view;

import com.saiba.app.review.view.test.TestDrive;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public final class DocReviewService {
    List<ReviewFolder> folders = new ArrayList();


    public DocReviewService() {
        super();
    }



    public void setFolders(List<ReviewFolder> folders) {
        this.folders = folders;
    }

    public List<ReviewFolder> getFolders() {
        return folders;
    }

    public static void main(String[] args) {
        DocReviewService docReviewService = new DocReviewService();
        docReviewService.setFolders(TestDrive.initFolders());
    }
}
