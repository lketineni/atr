package com.saiba.app.review.view;

import java.util.Date;

public final class ReviewFolder {
    String folderId;
    String folderName;
    String folderTypeId;
    String acronym;
    String description;
    int sortOrder = 0;
    boolean active = true;
    String updatedby;
    Date UpdatedDt;
    
    public ReviewFolder() {
        super();
    }

    public ReviewFolder(String folderId, String folderName, String folderTypeId, String acronym, String description,
                        int sortOrder, boolean active, String updatedby, Date UpdatedDt) {
        super();
        this.folderId = folderId;
        this.folderName = folderName;
        this.folderTypeId = folderTypeId;
        this.acronym = acronym;
        this.description = description;
        this.sortOrder = sortOrder;
        this.active = active;
        this.updatedby = updatedby;
        this.UpdatedDt = UpdatedDt;
    }


    public void setFolderId(String folderId) {
        this.folderId = folderId;
    }

    public String getFolderId() {
        return folderId;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderTypeId(String folderTypeId) {
        this.folderTypeId = folderTypeId;
    }

    public String getFolderTypeId() {
        return folderTypeId;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public void setUpdatedby(String updatedby) {
        this.updatedby = updatedby;
    }

    public String getUpdatedby() {
        return updatedby;
    }

    public void setUpdatedDt(Date UpdatedDt) {
        this.UpdatedDt = UpdatedDt;
    }

    public Date getUpdatedDt() {
        return UpdatedDt;
    }
}
