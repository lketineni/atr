package com.saiba.app.review.view.test;

import com.saiba.app.review.view.ReviewFolder;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class TestDrive {
    public TestDrive() {
        super();
    }
    
    
    public static List<ReviewFolder> initFolders(){
        List<ReviewFolder> folders = new ArrayList();
        
        folders.add(new ReviewFolder("fidOrignalApp","Original Application","type1","acronym","Original Application",0,true,"nvnguyen", Calendar.getInstance().getTime()));
        folders.add(new ReviewFolder("fid1stIssues","1st Issues Letter","type2","acronym","1st Issues Letter",0,true,"nvnguyen", Calendar.getInstance().getTime()));
        folders.add(new ReviewFolder("fid1stIssueAndResp","1st Issues Letter Reposne and Revised Application","type2","acronym","1st Issues Letter Reposne and Revised Application",0,true,"nvnguyen", Calendar.getInstance().getTime()));
        folders.add(new ReviewFolder("fid2ndIssues","2nd Issues Letter","type3","acronym","2nd Issues Letter",0,true,"nvnguyen", Calendar.getInstance().getTime()));
        folders.add(new ReviewFolder("fid2ndIssueAndResp","2nd Issues Letter Reposne and Revised Application","type3","acronym","2nd Issues Letter Reposne and Revised Application",0,true,"nvnguyen", Calendar.getInstance().getTime()));
        folders.add(new ReviewFolder("fid3rdtIssues","3rd Issues Letter","type4","acronym","3rd Issues Letter",0,true,"nvnguyen", Calendar.getInstance().getTime()));
        folders.add(new ReviewFolder("fid3ndIssueAndResp","3nd Issues Letter Reposne and Revised Application","type4","acronym","3nd Issues Letter Reposne and Revised Application",0,true,"nvnguyen", Calendar.getInstance().getTime()));
    
        return folders;
    }

}
